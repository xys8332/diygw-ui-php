## 🌈 DIYGW-UI-PHP

DIYGW-UI-PHP是一款基于thinkphp framework和 element admin开发而成的前后端分离系统。目的是结合现有diygw-ui打造一个后台API开发。

#### 💒 代码仓库

- <a target="_blank" href="https://gitee.com/diygw/diygw-ui-php">基于thinkphp6实现的DIYGW-UI-PHP</a>

#### 💒 集成前端

- <a href="https://gitee.com/diygw/diygw-ui-admin" target="_blank">https://gitee.com/diygw/diygw-ui-admin</a>


## ⚡ 功能
- [x] `用户管理` 后台用户管理
- [x] `部门管理` 配置公司的部门结构，支持树形结构
- [x] `岗位管理` 配置后台用户的职务
- [x] `菜单管理` 配置系统菜单，按钮等等
- [x] `角色管理` 配置用户担当的角色，分配权限
- [x] `数据字典` 管理后台表结构
- [x] `操作日志` 后台用户操作记录
- [x] `登录日志` 后台系统用户的登录记录



## 🌈 DIYGW可视化工具介绍

DIY官网可视化工具做好的可视化拖拽开发工具无须编程、零代码基础、所见即所得设计工具支持轻松在线可视化导出微信小程序、支付宝小程序、头条小程序、H5、WebApp、UNIAPP等源码 支持组件库,高颜值,卡片,列表,轮播图,导航栏,按钮,标签,表单,单选,复选,下拉选择,多层选择,级联选择,开关,时间轴,模态框,步骤条,头像,进度条等
丰富的按钮点击事件供选择、自定义方法调用、支持API在线调试、数据动态绑定、For循环数据绑定、IF判断绑定等

DIY官网可视化工具打造低代码可视化一键生成导出源码工具设计一次打通设计师+产品经理+技术开发团队必备低代码可视化工具。从想法到原型到源码，一步到位低代码生成源码工具

更多设计前往https://www.diygw.com 设计

> 运行环境要求PHP7.2+，兼容PHP8.1

### ⚡ 后台安装

使用宝塔新建网站指到public目录，新建完成后，浏览器输入域名即会提示安装。


### ⚡ 使用介绍

用户自定义表后，比如用户自定义的表user，你只需要在命令行输入php think diygw:table sys@User，会自动创建创建表相关Model、Controller等类。
其中sys表示应用目录，user表示某个表。如果想看更多命令请输出php think 可以查看更多快速创建类命令。